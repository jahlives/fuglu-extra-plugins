# -*- coding: UTF-8 -*-
from fuglu.shared import ScannerPlugin, DUNNO, string_to_actioncode, apply_template, FileList, extract_domain
from fuglu.extensions.dnsquery import lookup, DNSQUERY_EXTENSION_ENABLED
from fuglu.stringencode import force_bString
import re
import hashlib
import os



class EBLLookup(ScannerPlugin):
    def __init__(self,config,section=None):
        ScannerPlugin.__init__(self,config,section)
        self.logger=self._logger()
        
        self.whitelist = None
        self.domainlist = None
        
        self.requiredvars={
            'whitelist_file':{
                'default':'',
                'description':'path to file containing whitelisted sender domains',
            },
            'dnszone':{
                'default':'ebl.msbl.org',
                'description':'the DNS zone to query. defaults to ebl.msbl.org',
            },
            'hash': {
                'default':'sha1',
                'description':'hash function used by DNS zone. Use one of md5, sha1, sha224, sha256, sha384, sha512'
            },
            'response':{
                'default':'127.0.0.2',
                'description':'expected response of zone query',
            },
            'action':{
                'default':'dunno',
                'description':'action on hit (dunno, reject, defer, delete). if set to dunno will tag as spam. do not use reject/defer in after queue mode',
            },
            'messagetemplate':{
                'default':'${sender} listed by ${dnszone} : ${message}',
                'description':'reject message template',
            },
            'maxlookups':{
                'default':'10',
                'description':'maximum number of email addresses to check per message',
            },
            'check_always':{
                'default':'False',
                'description':'set to True to check every suspect. set to False to only check mail that has not yet been classified as spam or virus',
            },
            'normalisation':{
                'default':'ebl',
                'description':'type of normalisation to be applied to email addresses before hashing. choose one of ebl (full normalisation according to ebl.msbl.org standard), low (lowercase only)'
            },
            'domainlist_file':{
                'default':'',
                'description':'path to file containing a list of domains. if specified, only query email addresses in these domains.'
            },
        }


    
    def _is_whitelisted(self, from_domain):
        whitelist_file = self.config.get(self.section,'whitelist_file').strip()
        if whitelist_file == '':
            return False
        
        if self.whitelist is None:
            self.whitelist = FileList(whitelist_file, lowercase=True)
        
        whitelisted = False
        if from_domain in self.whitelist.get_list():
            whitelisted = True
            
        return whitelisted
    
    
    
    def _do_lookup(self, email_address):
        domainlist_file = self.config.get(self.section,'domainlist_file').strip()
        if domainlist_file == '':
            return True
        
        if self.domainlist is None:
            self.domainlist = FileList(domainlist_file, lowercase=True)
        
        in_domainlist = False
        domain = extract_domain(email_address)
        if domain in self.domainlist.get_list():
            in_domainlist = True
        
        return in_domainlist
        
        
        
    def _email_normalise_ebl(self, address):
        if not '@' in address:
            self.logger.error('Not an email address: %s' % address)
            return address
        
        address = address.lower()
        
        lhs, domain = address.rsplit('@',1)
        domainparts = domain.split('.')
        
        if 'googlemail' in domainparts: # replace googlemail with gmail
            tld = '.'.join(domainparts[1:])
            domain = 'gmail.%s' % tld
            domainparts = ['gmail', tld]
        
        if '+' in lhs: # strip all '+' tags
            lhs = lhs.split('+')[0]
            
        if 'gmail' in domainparts: # discard periods in gmail
            lhs = lhs.replace('.', '')
            
        if 'yahoo' in domainparts or 'ymail' in domainparts: # strip - tags from yahoo
            lhs = lhs.split('-')[0]
            
        lhs = re.sub('^(envelope-from|id|r|receiver)=', '', lhs) # strip mail log prefixes
            
        return '%s@%s' % (lhs, domain)
    
    
    
    def _email_normalise_low(self, address):
        address = address.lower()
        return address
    
    
    
    def _email_normalise(self, address):
        n = self.config.get(self.section,'normalisation')
        if n == 'ebl':
            address = self._email_normalise_ebl(address)
        elif n == 'low':
            address = self._email_normalise_low(address)
        return address
    
    
    
    def _create_hash(self, value):
        hashtype = self.config.get(self.section,'hash').lower()
        
        if hasattr(hashlib, 'algorithms_guaranteed'):
            algorithms = hashlib.algorithms_guaranteed
        else:
            algorithms = ['md5', 'sha1'] #python 2.6
        
        if hashtype in algorithms:
            hasher = getattr(hashlib, hashtype)
            myhash = hasher(force_bString(value)).hexdigest()
        else:
            myhash = ''
        return myhash
    
    
    
    def _ebl_lookup(self, addr_hash):
        listed = False
        message = None
        
        dnszone = self.config.get(self.section,'dnszone')
        response = self.config.get(self.section,'response')
        query = '%s.%s' % (addr_hash, dnszone)
        result = lookup(query)
        if result is not None:
            for rec in result:
                self.logger.debug('response for %s is %s' % (query, rec))
                if rec == response:
                    listed = True
                    result = lookup(query, qtype='TXT')
                    if result:
                        message = result[0]
                    break
        else:
            self.logger.debug('no response for %s' % query)
                
        return listed, message
    
    
    
    def examine(self, suspect):
        if not DNSQUERY_EXTENSION_ENABLED:
            return DUNNO
        
        if not self.config.getboolean(self.section,'check_always'):
            # save the lookup if mail is already tagged as virus or spam
            if suspect.is_virus() or suspect.is_spam() or suspect.is_blocked():
                return DUNNO
            
        if self._is_whitelisted(suspect.from_domain):
            return DUNNO
        
        maxlookups = self.config.getint(self.section, 'maxlookups')
        emails = suspect.get_tag('emails',defaultvalue=[])
        emails = [self._email_normalise(email) for email in emails]
        emails = list(set(emails))
        
        #if emails:
        #    self.logger.debug('%s EBL checking addresses %s' % (suspect.id, ', '.join(emails)))
        
        listed = False
        action = DUNNO
        message = None
        email = None
        for email in emails:
            if maxlookups==0:
                break
            if self._do_lookup(email):
                maxlookups-=1
                addr_hash = self._create_hash(email)
                self.logger.debug('%s looking up email address %s with hash %s' % (suspect.id, email, addr_hash))
                listed, message = self._ebl_lookup(addr_hash)
                if listed:
                    break

        suspect.tags['spam']['EBL'] = listed
        
        if listed:
            self.logger.info('%s EBL hit for %s' % (suspect.id, email))
            action = string_to_actioncode(self.config.get(self.section, 'action'))
            suspect.tags['EBL.email'] = email
            suspect.tags['EBL.reason'] = message
            if action != DUNNO:
                values = {
                    'dnszone': self.config.get(self.section,'dnszone'),
                    'message': message,
                }
                message = apply_template(self.config.get(self.section,'messagetemplate'),suspect, values)
        
        return action, message
    
    
    
    def lint(self):
        dnszone = self.config.get(self.section,'dnszone')
        print('INFO: querying zone %s' % dnszone)
        
        lint_ok = True
        if not self.check_config():
            print('ERROR: Error checking config')
            lint_ok = False
            
        if not DNSQUERY_EXTENSION_ENABLED:
            print("ERROR: no DNS resolver library available - this plugin will do nothing")
            lint_ok = False
        
        
        if hasattr(hashlib, 'algorithms_guaranteed'):
            algorithms = hashlib.algorithms_guaranteed
        else:
            algorithms = ['md5', 'sha1'] #python 2.6
            print('WARNING: old version of hashlib, consider upgrade')
            
        hashtype = self.config.get(self.section,'hash').lower()
        if hashtype not in algorithms:
            lint_ok = False
            print('ERROR: unsupported hash type %s' % hashtype)
            
        normalisation = self.config.get(self.section,'normalisation')
        if normalisation not in ['ebl', 'low']:
            lint_ok = False
            print('ERROR: unsupported normalisation type %s' % normalisation)
            
        if lint_ok:
            addr_hash = self._create_hash('noemail@example.com')
            listed, message = self._ebl_lookup(addr_hash)
            if not listed:
                print('WARNING: test entry not found in dns zone')
            else:
                print('OK: test entry found in dns zone: %s' % message)
            
        if lint_ok:
            whitelist_file = self.config.get(self.section,'whitelist_file')
            if whitelist_file.strip() == '':
                print('INFO: No whitelist defined')
            else:
                if not os.path.exists(whitelist_file):
                    print('ERROR: whitelist file %s does not exist' % whitelist_file)
                    lint_ok = False
                
        if lint_ok:
            domainlist_file = self.config.get(self.section,'domainlist_file')
            if domainlist_file.strip() == '':
                print('INFO: No domainlist defined')
            else:
                if not os.path.exists(domainlist_file):
                    print('ERROR: domainlist file %s does not exist' % domainlist_file)
                    lint_ok = False
                
        return lint_ok