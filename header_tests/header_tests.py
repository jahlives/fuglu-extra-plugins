# -*- coding: UTF-8 -*-
#  Copyright (c) 2019 by Tobi <jahlives@gmx.ch>
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#

from fuglu.shared import ScannerPlugin, DUNNO
import re


class HeaderTests(ScannerPlugin):
    """
    HeaderTests class

    runs defined regex patterns against headers in the message
    can mark a message as virus via fuglu virus status
    """

    def __init__(self, config, section=None):
        ScannerPlugin.__init__(self, config, section)
        self.requiredvars = {
            'returnonhit': {
                'default': 'True',
            },

            'enginename': {
                'default': 'AI-Detection',
            },

        }
        self.logger = self._logger()
        self.enginename = self.config.get(self.section, 'enginename')

    def _run_test(self, suspect, headers, headers_sa, regex='', action='', headername=''):
        """
        Test the header for regex

        :param Suspect suspect: fuglususpect
        :param str headers: original message headers
        :param str headers_sa: spamassassin message headers
        :param str regex: pattern to search
        :param str action: will be returned
        :param str headername: header to match pattern
        :return: action or None
        """
        regexname = regex
        regex_header = None
        # some sanity checks first
        # compile regex pattern
        if (headername == 'SpamassassinReport' and regexname != '') or (
                headername is None or headername == '' and regexname != ''):
            try:
                regex = re.compile(regexname)
            except Exception as e:
                self.logger.info('could not compile %s: %s' % (regexname, str(e)))
                return None
        # compile headername pattern
        elif headername != '' and headername is not None and headername != 'SpamassassinReport':
            try:
                regex_header = re.compile('^' + headername + ': ', re.MULTILINE)
            except Exception as e:
                self.logger.info('could not compile regex for headername %s: %s' % (headername, str(e)))
                return None
            if regexname != '' and regexname is not None:
                try:
                    regex = re.compile('^' + headername + ': ' + regexname, re.MULTILINE)
                except:
                    self.logger.info('could not compile ^%s: %s' % (headername, regexname))
                    return None
        elif headername == 'SpamassassinReport' and (regexname == '' or regexname is None):
            self.logger.info('testing SpamassassinReport only works with regex setting in config')
            return None
        else:
            self.logger.info('oops should not come here seems a uncaught error')
            return None

        # search in spamassassin report tag
        if headername == 'SpamassassinReport' or headername is None or headername == '':
            if headers_sa is not None:
                if re.search(regex, headers_sa):
                    self.logger.info('suspect %s marked as infected matching /%s/' % (suspect.id, regex.pattern))
                    return action
                else:
                    self.logger.debug('suspect %s not matched /%s/' % (suspect.id, regex.pattern))
            else:
                self.logger.info('suspect %s could not find SAPlugin.report' % suspect.id)

        # test headername content against the pattern
        elif type(headername) == str and regexname != '' and regexname is not None:
            if not regex_header.search(headers):
                self.logger.info('suspect %s could not find header %s in msg. Skip processing'
                                 % (suspect.id, headername))
            elif regex.search(headers):
                self.logger.info('suspect %s marked as infected matching /%s/ in header %s'
                                 % (suspect.id, regex.pattern, headername))
                return action
            else:
                self.logger.debug('suspect %s could not find /%s/ in header %s'
                                  % (suspect.id, regex.pattern, headername))

        # test if header exists
        elif type(headername) == str and (regexname == '' or regexname is None):
            if not regex_header.search(headers):
                self.logger.debug('suspect %s could not find header %s in msg. Skip processing'
                                  % (suspect.id, headername))
            elif regex_header.search(headers):
                self.logger.info('suspect %s marked as infected in header %s'
                                 % (suspect.id, headername))
                return action
            else:
                self.logger.debug('suspect %s could not find header %s' % (suspect.id, headername))
        else:
            self.logger.info('suspect %s could not find regex setting. Skip processing' % suspect.id)
        return None

    def _fetch_config(self, name):
        """
        read params from config file

        :param str name: config prefix
        :return dict: key=>value pairs for self.runTest()
        """
        ret = {}
        values = ['regex', 'action', 'headername']
        for option in values:
            if self.config.get(self.section, name + '_' + option):
                ret[option] = self.config.get(self.section, name + '_' + option)
        return ret

    def examine(self, suspect):
        """
        :param Suspect suspect: fuglususpect
        :return str: fuglu action DUNNO
        """
        # store spamassasin report tag content so we do not read it for every test
        headers_sa = None
        headers = suspect.get_headers()
        try:
            headers_sa = suspect.get_tag('SAPlugin.report')
        except Exception:
            self.logger.info('suspect %s could not find SAPlugin.report' % suspect.id)

        checks = self.config.get(self.section, 'checks').split()
        if isinstance(checks, dict):
            self.logger.error('cannot find configuration')
            return DUNNO
        for check in checks:
            # unpack the makeTestConfig() dict to provide parameters for runTest()
            config = self._fetch_config(check)
            hdr = self._run_test(suspect, headers, headers_sa, **config)

            if hdr is not None:
                if hdr == 'virus' and not suspect.is_virus():
                    suspect.set_tag('virus', {'header_tests': True})
                    suspect.set_tag('%s.virus' % 'InSaneSecurity-AI',
                                    {'message content': 'SEXTORTION'})
                if self.config.get(self.section, check + '_header2write') != ''\
                        and self.config.get(self.section, check + '_header2writeValue') != '':

                    if not self.config.getboolean(self.section, check + '_internalheader'):
                        suspect.add_header(self.config.get(self.section, check + '_header2write'),
                                           self.config.get(self.section, check + '_header2writeValue'), immediate=True)
                    else:
                        suspect.add_header(self.config.get(self.section, check + '_header2write'),
                                           self.config.get(self.section, check + '_header2writeValue'))

                if not self.config.getboolean(self.section, 'returnonhit')\
                        or not self.config.getboolean(self.section, check + '_' + 'returnonhit')\
                        or self.config.getboolean(self.section, 'returnonhit')\
                        or self.config.getboolean(self.section, check + '_' + 'returnonhit'):
                    return DUNNO
        return DUNNO

    def lint(self):
        allok = (self.checkConfig() and self.lint_regex())
        return allok

    def lint_regex(self):
        error = False
        if not self.config.get(self.section, 'checks') or self.config.get(self.section, 'checks') == '':
            print('WARNING: no tests configured. This plugin will do nothing')
            return True
        for check in self.config.get(self.section, 'checks').split():
            regex = self.config.get(self.section, check + '_regex')
            _ = self.config.get(self.section, check + '_action')
            headername = self.config.get(self.section, check + '_headername')
            _ = self.config.get(self.section, check + '_header2write')
            _ = self.config.get(self.section, check + '_header2writeValue')
            if regex is not None and regex != '':
                try:
                    re.compile(regex)
                    print('successfully compiled pattern %s_regex %s' % (check, regex))
                except Exception as e:
                    print('problem with config: could not compile regex pattern %s: %s' % (regex, str(e)))
                    error = True
            if headername is not None and headername != '' and headername != 'SpamassassinReport':
                try:
                    re.compile(headername)
                    print('successfully compiled pattern %s_headername %s' % (check, headername))
                except Exception as e:
                    print('problem with config: could not compile regex pattern %s: %s' % (headername, str(e)))
                    error = True
            if error:
                return False
            # regex not set headername must be set and not SpamassassinReport
            if not regex:
                if headername is None:
                    print('problem with config: %s_headername must be set for %s'
                          % (check, self.config.get(self.section, check + '_headername')))
                    error = True
                elif headername == '' or headername == 'SpamassassinReport':
                    print('problem with config: %s_regex must be set for %s'
                          % (check, self.config.get(self.section, check + '_headername')))
                    error = True
                elif headername != '' and headername != 'SpamassassinReport':
                    print('problem with config: %s_regex must be set for %s'
                          % (check, self.config.get(self.section, check + '_headername')))
                    error = True
            else:
                if headername == '':
                    print('problem with config: %s_headername must be set for %s'
                          % (check, self.config.get(self.section, check + '_regex')))
                    error = True
        return not error
