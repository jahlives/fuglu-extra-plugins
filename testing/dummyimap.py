class DummyIMAP(object):
    """Dummy IMAP server returning predefined answers from a dict"""
    idCounter = 0

    def __init__(self, communication_dict, custom_identifier=None):
        """
        Constructor

        Args:
            communication_dict (dict): dict with predefined answers
            custom_identifier (str): identifier for the instance (useful if several are used)
        """
        self._comDict = communication_dict.copy()
        if custom_identifier:
            identifier = custom_identifier
        else:
            DummyIMAP.idCounter += 1
            identifier = str(DummyIMAP.idCounter)

        self._name = "DummyIMAP("+identifier+")"
        self.debug = False
        self.append_cmd_ignorecontent = False
        self.append_store = False

        self.append_messages = []

    def _generic(self, callee, args_list):
        """
        The main generic routine parsing the requests and returning values from the dict

        Args:
            callee (str): imap function called
            args_list (list): a list of function arguments

        Returns:
            list: the answer from the dict

        """
        if self.debug:
            print("DummyIMAP: (callee) : "+str(callee))
            print("DummyIMAP: (args)   : "+str(args_list))

        key = callee + "::"+str(args_list)
        key_list = self._comDict.get(key)
        assert key_list is not None
        assert len(key_list) > 0
        generic_answer = key_list.pop()
        if self.debug:
            print("DummyIMAP: (answer) : "+str(generic_answer))
        return generic_answer

    def constructor_imap4(self, host, port):
        """
        The constructor function, can be used for the mock because
        the real constructor and internal setup is done before
        Args:
            host (str): hostname
            port (int): port

        Returns:
            DummyIMAP: a reference to itself

        """
        self._generic("IMAP4", [host, port])
        return self

    def login(self, username, password):
        """Implements a fully dummy imap - login command ignoring username/password"""
        assert username
        assert password
        return self._generic("login", ["username", "password"])

    def list(self):
        """Implements a imap - list - command"""
        return self._generic("list", [])

    def logout(self):
        """Implements a imap - logout - command"""
        return self._generic("logout", [])

    def select(self, mailbox_folder):
        """Implements a imap - select - command"""
        return self._generic("select", [mailbox_folder])

    def status(self, mailbox_folder, key_string):
        """Implements a imap - status - command"""
        return self._generic("status", [mailbox_folder, key_string])

    def close(self):
        """Implements a imap - close - command"""
        return self._generic("close", [])

    def uid(self, command, args, infilter):
        """Implements a imap - uid - command"""
        return self._generic("uid", [command, args, infilter])

    def getquota(self, root):
        """Implements a imap - getquota - command"""
        return self._generic("getquota", [root])

    def expunge(self):
        """Implements a imap - expunge - command"""
        return self._generic("expunge", [])

    def append(self, mailbox, flags, date_time, message):
        """
        Implements a imap - append - command.

        Based on settings this might store the messages and
        ignore the message in the command.
        (if self.append_cmd_ignorecontent is False the full message
        has to be stored in the command dict to be recognised)

        Args:
            mailbox (str): mailbox folder
            flags (str): the imap flags
            date_time (time): always ignored
            message (str/bytes): the message to be appended

        Returns:
            list: answer stored in dict
        """
        # ignore time
        # ignore message content when checking for the
        # command in the dict

        if self.append_store:
            self.append_messages.append(message)

        return self._generic("append", [mailbox, flags, None,
                                        message if not self.append_cmd_ignorecontent else ""])

    def is_empty(self):
        """Check if all commands in dict have been consumed"""
        dict_is_empty = True
        for key, value in iter(self._comDict.items()):
            if len(value) > 0:
                print("%s: Key: %s -> still has %u list elements:" % (self._name, key, len(value)))
                for elem in value:
                    print("      -> %s" % elem)
                dict_is_empty = False
        return dict_is_empty
