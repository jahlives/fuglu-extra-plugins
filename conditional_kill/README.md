This plugin aims to be a "more failsafe" approach for a DELETE message plugin than the already existing killer plugin.
killer plugin deletes EVERY message except the plugin is skipped by a rule in skipplugin.regex for a certain message.
So if the rule for skip plugin does for whaterver reason not match **!!! EVERY !!!** mail wil be deleted.

Here comes the different approach of this plugin which only deletes a message if a rule explicitly matches the message

Installation
---
 * copy the plugin file to your `plugindir` (defined in fuglu.conf) - usually `/usr/local/fuglu/plugins`
 * load the plugin in fuglu.conf  - for example: `plugins=[...], conditional_killer.KillerRulePlugin`
 * adjust, rename `killrules.regex.dist` to `killrules.regex` and save it as `/etc/fuglu/killrules.regex`
 * run `fuglu --lint` to test the config
 * reload or restart fuglu
 
 Configuration
 ---
 A rule consists of a `tag`, a `regex` and an `action` \
 A tag can be everything that fuglu `SuspectFilter` knows
 
 For example
 
 `from_address      /MyRegex/   DELETE`
 
 the `action` **NO** can be used to exclude a suspect from deletion
 
 `from_address      /^user@domain\.tld$/  NO` \
 `from_address       /@domain\.tld$/     DELETE` 